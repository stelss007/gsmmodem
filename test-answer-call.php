<?php
/**
 * Created by PhpStorm.
 * User: rus
 * Date: 30.01.17
 * Time: 16:22
 */
include_once('./DtmfDecoder.php');
include_once('./GsmModem.php');

include_once('./PDU/InvalidArgumentException.php');
include_once('./PDU/Utf8.php');
include_once('./PDU/PduClass.php');
include_once('./PDU/PduFactory.php');

use StelsSoft\GSM\GsmModem;

class TestModem
{
    private $dtmfDecoder;
    private $modem;
    private $soundAt;
    private $menu;
    private $soundOutHandle;
    private $prevMenu;
    private $toPlay;

    public function __construct()
    {
        $this->modem = new GsmModem();
        //$this->modem->debugModeOn();

        $this->dtmfDecoder = new DtmfDecoder();

        # данная команда включает в модеме отображение номера звонящего
        $this->modem->command->set("AT+CLIP=1", '/^(OK|ERROR)/');

        $this->menu = include('./menu.php');

        $this->currentMenu = $this->menu;
        $this->prevMenu = $this->currentMenu;
        if (isset($this->currentMenu['menu'])) {
            $this->toPlay = $this->currentMenu['menu'];
        }

        while (true) {
            $this->modem->waitIncomingСall();
            echo PHP_EOL . 'Звонок: ';
            echo $this->modem->getIncomingPhoneNumber();
            echo PHP_EOL . 'Берем трубку : ';
            echo $this->modem->answerThePhone();

//            $this->modem->callNumber('+380978803826');
            echo PHP_EOL . 'Ожидаем соединения : ';
            echo $this->modem->waitCallConnect();
            echo PHP_EOL . 'Переключаем модем в режим приема/передачи голоса : ';
            echo $this->modem->setVoiceMode();

            $this->acceptCall();
        }
    }

    private function playMenu()
    {
        if(feof($this->soundOutHandle)) {

            if($this->toPlay) {
                $currentItem = array_shift($this->toPlay);
                $this->soundOutHandle = fopen($currentItem['title_voice'], 'r+b');
            } else {
                $this->soundOutHandle = fopen($this->menu['standart_messages']['back']['title_voice'], 'r+b');
            }

        }

        $sndData = fread($this->soundOutHandle, 320);
        $this->modem->voice->write($sndData);
    }

    private function checkMenu($dtmf)
    {
        switch ($dtmf) {
            case '*':
                $this->currentMenu = $this->prevMenu;
                $this->soundOutHandle = fopen($this->currentMenu['info_voice'], 'r+b');
                $this->toPlay = [];

                if (isset($this->currentMenu['menu'])) {
                    $this->toPlay = $this->currentMenu['menu'];
                }
                break;
            case '#':
                $this->currentMenu = $this->menu;
                $this->soundOutHandle = fopen($this->currentMenu['info_voice'], 'r+b');
                $this->toPlay = [];

                if (isset($this->currentMenu['menu'])) {
                    $this->toPlay = $this->currentMenu['menu'];
                }
                break;
            default:
                if (isset($this->currentMenu['menu'][$dtmf])) {
                    $this->prevMenu = $this->currentMenu;
                    $this->currentMenu = $this->currentMenu['menu'][$dtmf];

                    $this->soundOutHandle = fopen($this->currentMenu['info_voice'], 'r+b');
                    $this->toPlay = [];

                    if (isset($this->currentMenu['menu'])) {
                        $this->toPlay = $this->currentMenu['menu'];
                    }

                    //$this->playMenu();
                }
                break;
        }
    }

    private function acceptCall()
    {
        $recordFileName = 'records/call-'.date('Y-m-d-h-i');

        $this->soundOutHandle = fopen($this->currentMenu['info_voice'], 'r+b');
        $recordCall= fopen($recordFileName.'.raw', 'w+b');

        $before = microtime(true);

        while(true) {

            $this->playMenu();

            $inData = $this->modem->voice->read(320);

            //fwrite($recordCall, $inData);

            $dtmf = $this->dtmfDecoder->decode($inData);

            if ($dtmf) {
                echo PHP_EOL.'Нажата кнопка: '.$dtmf;

                $this->checkMenu($dtmf);
            }

            # мониторим состояние звонка
            # выходим если сброшен
            if ($this->modem->checkConnection() === "CEND") {
                $convertCommand = 'sox -b 16 -r 8000 -e signed-integer ' .$recordFileName. '.raw ' .$recordFileName. '.flac gain -n -5 silence 1 5 2%';
                //echo PHP_EOL.$convertCommand;
                shell_exec($convertCommand);
                echo PHP_EOL.'Вызов завершон';
                break;
            }

            while(microtime(true)-$before < 0.02 ) { }
            $before = microtime(true);
        }

        fclose($recordCall);
        $this->modem->endCall();
    }
}


new TestModem();