<?php

include_once('./DtmfDecoder.php');
include_once('./Networks.php');
include_once('./GsmModem.php');

include_once('./PDU/InvalidArgumentException.php');
include_once('./PDU/Utf8.php');
include_once('./PDU/PduClass.php');
include_once('./PDU/PduFactory.php');

use StelsSoft\GSM\GsmModem;

$modem = new GsmModem();
$modem->debugModeOn();

//Отправка сообщений в виде текста (Не очень удобная штука, только латиница)
$modem->smsSendAsTxt('+380978803826', 'hello');
//$modem->smsSendAsTxt('+380979971858', 'hello');

//Отправка сообщений в виде PDU (Удобная штука, поддержка UTF-8)
//$modem->smsSendAsPdu('+380979971858', 'Привет Рус!');
//$modem->smsSendAsPdu('+380978803826', 'Привет Рус!');
$modem->smsSendAsPdu('+380935063780', 'Привет Рус!', 'flash');

