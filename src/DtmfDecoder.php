<?php
namespace StelsSoft\GSM;

class DtmfDecoder
{
    /**
     * Хэш массив в котором будут храниться расчитанные коэфициенты
     * необходимые для работы алгоритма Гёрцеля
     * @var array
     */
    private $frequency ;

    // список частот строк и столбцов таблицы dtmf сигналов
    //          1209 Гц   1336 Гц   1477 Гц   1633 Гц
    // 697 Гц   1         2         3         A
    // 770 Гц   4         5         6         B
    // 852 Гц   7         8         9         C
    // 941 Гц   *         0         #         D

    /**
     * Список частот строк таблицы dtmf сигналов
     *
     * @var array
     */
    private $rowFrequency;

    /**
     * Список частотстолбцов таблицы dtmf сигналов
     *
     * @var array
     */
    private $colFrequency;


    /**
     * Вспомогательный хэш массив для определения номера dtmf сигнала
     * @var array
     */
    private $dtmfTable;



    /**
     * Список наименований dtmf сигналов (входным параметром является
     * значение из вспомогательного массивa dtmf)
     * выходным - наименование нажатой кгнопки
     * @var array
     */
    private $info;


    /**
     * Расчитывается. Содержит количество обрабатываемых частот
     *
     * @var int
     */
    private $tones;

    /**
     * Частота оцифровки обрабатываемого сигнала
     *
     * @var int
     */
    private $rate;

    /**
     * Количество оцифровок обрабатываемых за раз
     * значение выбранно из расчета обработать за раз пакет из 320 байт
     * считанных из аудиопорта модема (2 байта на 1 оцифровку)
     * (уменьшил до 100 для увеличения скорости обработки)
     *
     * @var int
     */
    private $len;

    /**
     * Используется для приведения значений максимальной мощности
     *
     * @var float
     */
    private $range;

    /**
     * Используется для отсекания сигналов с мощностью менше указанной
     *
     * @var int
     */
    private $thresh;

    /**
     * Минимальное количество пакетов в которых фиксируется нажатие кнопки
     *
     * @var int
     */
    private $minCount;


    /**
     * Массив содержащий временные данные работы алгоритма
     *
     * @var array
     */
    private $temp;


    /**
     */
    public function __construct()
    {
        $this->frequency = [
            '697' => ['K' => 0, 'C' => 0],
            '770' => ['K' => 0, 'C' => 0],
            '852' => ['K' => 0, 'C' => 0],
            '941' => ['K' => 0, 'C' => 0],
            '1209' => ['K' => 0, 'C' => 0],
            '1336' => ['K' => 0, 'C' => 0],
            '1477' => ['K' => 0, 'C' => 0],
            '1633' => ['K' => 0, 'C' => 0],
        ];

        // список частот строк и столбцов таблицы dtmf сигналов
        //          1209 Гц   1336 Гц   1477 Гц   1633 Гц
        // 697 Гц   1         2         3         A
        // 770 Гц   4         5         6         B
        // 852 Гц   7         8         9         C
        // 941 Гц   *         0         #         D
        $this->rowFrequency = [ '697', '770', '852', '941' ];
        $this->colFrequency = [ '1209', '1336', '1477', '1633' ];


        /**
         * Вспомогательный хэш массив для определения номера dtmf сигнала
         */
        $this->dtmfTable = [
            '697' => [ '1209' => 1, '1336' => 2, '1477' => 3, '1633' => 4 ],
            '770' => [ '1209' => 5, '1336' => 6, '1477' => 7, '1633' => 8  ],
            '852' => [ '1209' => 9, '1336' => 10, '1477' => 11, '1633' => 12  ],
            '941' => [ '1209' => 13, '1336' => 14, '1477' => 15, '1633' => 16  ],
        ];

        /**
         * Список наименований dtmf сигналов (входным параметром является
         * значение из вспомогательного массивa dtmf)
         * выходным - наименование нажатой кгнопки
         */
        $this->info = ['NONE', '1', '2', '3', 'A', '4', '5', '6', 'B', '7', '8', '9', 'C', '*', '0', '#', 'D'];

        // далее идут параметры используемые для работы алгоритма
        $this->tones = 0;   // расчитывается. ссодержит количество обрабатываемых частот
        $this->rate = 8000; // частота оцифровки обрабатываемого сигнала
        $this->len = 100;   // количество оцифровок обрабатываемых за раз
        // значение выбранно из расчета обработать за раз пакет из 320 байт
        // считанных из аудиопорта модема (2 байта на 1 оцифровку)
        // (уменьшил до 100 для увеличения скорости обработки)
        $this->range = 0.15;      // используется для приведения значений максимальной мощности
        $this->thresh = 99999999; // используется для отсекания сигналов с мощностью менше указанной
        $this->minCount = 4;      // минимальное количество пакетов в которых фиксируется нажатие кнопки
        // для того чтобы алгоритм считал кнопку нажатой
        // range, thresh и minCount подбирались опытным путем и тестировались
        // нескольких десятков звуковых файлов содержащих dtmf сигналы и посторонние
        // шумовые эффекты.

        // Массив содержащий временные данные работы алгоритма
        $this->temp = [
            'minCount' => 0,
            'sample' => [],
            'power' =>  [],
            'maxPower' => 0,
            'thresh' => 0,
            'on'    =>  [],
            'lastDtmf' => ''
        ];

        $this->recalc();
    }

    /**
     * Данная функция производит предварительный расчет мощностей гармоник(указанных частот)
     *
     * @param array $freqList
     */
    private function calcPower($freqList)
    {
        $u0 = [];
        $u1 = [];
        $i = 0;

        foreach($freqList as $key => $value) {
            $u0[$value] = 0;
            $u1[$value] = 0;
        }

        while ($i < $this->len) {   # feedback
            if (isset($this->temp['sample'][$i])) {
                $in = $this->temp['sample'][$i];
            } else {
                $in = 0;
            }  # >> 7;

            foreach($freqList as $key => $value) {
                $t = $u0[$value];
                $u0[$value] = $in + $this->frequency[$value]['C'] * $u0[$value] - $u1[$value];
                $u1[$value] = $t;
            }
            $i++;
        }


        foreach($freqList as $key => $value) {
            $this->temp['power'][$value] = $u0[$value]
                * $u0[$value]
                + $u1[$value]
                * $u1[$value]
                - $this->frequency[$value]['C']
                * $u0[$value]
                * $u1[$value];

            if ($this->temp['power'][$value] > $this->temp['maxPower']) {
                $this->temp['maxPower'] = $this->temp['power'][$value];
            }

        }
    }

    /**
     * Данная функция производит предварительный расчет коэффициентов необходимых для работы алгоритма
     */
    private function recalc()
    {
        $this->tones = count($this->frequency);

        foreach($this->frequency as $key => $value) {
            $this->frequency[$key]['K'] = $this->len * $key / $this->rate;
            $this->frequency[$key]['C'] = 2 * cos( 2 * 3.14159265 * $this->frequency[$key]['K'] / $this->len);
        }
    }


    /**
     * Данная функция отсекает пакеты с мощьностью сигнала ниже $this->temp[maxPower]
     * расчитывает проходной уровень мощности для частот
     * и фиксирует частоты уровень мощности которых выше проходного в массиве $this->temp[on][$f]
     *
     * @param array $freqList
     * @return int
     */
    private function midleCalc($freqList)
    {
        $this->calcPower($freqList);

        if ($this->temp['maxPower'] < $this->thresh){
            return 0 ;
        }

        $this->temp['thresh'] = $this->range  * $this->temp['maxPower'];
        $on_count = 0;

        foreach($freqList as $key => $value) {
            if ($this->temp['power'][$value] > $this->temp['thresh']) {
                $this->temp['on'][$value] = 1;
                $on_count++;
            } else {
                $this->temp['on'][$value] = 0;
            }
        }

        return $on_count;
    }

    /**
     * Данная функция производит проверку наличия 2-х частот в обработанном пакете
     * 1-й частоты из группы частот означающих номер строки из таблицы dtmf
     * и 1-й частоты из группы частот означающих номер колонки из таблицы dtmf
     * если проверка пройдена - возвращает значение из массива dtmf (номер)
     *
     * @return int
     */
    private function preDecode()
    {
        $row_count = $this->midleCalc($this->rowFrequency);

        if(!$row_count) {
            return 0;
        }

        $col_count = $this->midleCalc($this->colFrequency) + 1;

        if(!$col_count) {
            return 0;
        }

        if($row_count === 1 && $col_count === 1) {
            return 0;
        }

        foreach($this->rowFrequency as $key => $dtmf) {
            if ($this->temp['on'][$dtmf]) {
                foreach($this->colFrequency as $key2 => $f) {
                    if ($this->temp['on'][$f]) {
                        return $this->dtmfTable[$dtmf][$f];
                    }
                }
            }
        }

        return 0;
    }

    /**
     * Данная функция производит финальную проверку наличия dtmf сигнала
     * основываясь на его длительности (minCount) отсекая случайные срабатывания
     * и возвращает название нажатой кнопки из массива info
     *
     * @return int|mixed
     */
    private function analise()
    {
        $x = $this->preDecode();
        $this->tmpClear();
        //return $x;

        if ($x && $this->temp['lastDtmf'] !== 0 && $x === $this->temp['lastDtmf']){
            $this->temp['minCount']++;
        } else {

            if ( $this->temp['lastDtmf'] && $x !== $this->temp['lastDtmf'] ) {

                if ($this->temp['minCount'] >= $this->minCount){
                    $r = $this->temp['lastDtmf'];
                    $this->temp['lastDtmf'] = $x;

                    return $r;
                }
            }

            $this->temp['minCount'] = 0;
        }

        $this->temp['lastDtmf'] = $x;
        
        return 0;
    }

    /**
     * Cлужебная функция для отчистки результатов промежуточных расчетов
     * вызывается после расчетов мощьностей для каждого пакета
     */
    private function tmpClear()
    {
        $this->temp['sample'] = [];
        $this->temp['power'] = [];
        $this->temp['maxPower'] = 0;
        $this->temp['on'] = [];
        $this->temp['thresh'] = 0;
    }

    /**
     * Функция принимающая на обработку пакет аудиоданных
     * и возвращающая название нажатой кнопки в случае
     * обнаружения dtmf сигнала
     * @param string $data
     * @return bool|mixed
     */
    public function decode($data)
    {
        $a = unpack('s'.$this->len, $data);
        $this->temp['sample'] = $a;
        $x = $this->analise();

        if ($x) {
            return $this->info[$x];
        }

        return false;
    }
}