<?php
namespace StelsSoft\GSM;

use Exception;
use StelsSoft\PDU\PduFactory;

/**
 * Class GsmModem
 */
class GsmModem
{
    /**
     * Флаг режима отладки
     *
     * @var boolean
     */
    private $debug;

    /**
     * Интерфейс для работы с AT коммандами модема
     *
     * @var GsmAtCommand
     */
    public $command;

    /**
     * Интерфейс для работы с звуковым потоком модема
     *
     * @var GsmVoice
     */
    public $voice;

    // при подключении модема к компьютеру с OS Linux
    // создаются 3 usb интерфейса для обмена данными с модемом
    // обычно это:
    // /dev/ttyUSB0 - командный интерфейс модема
    // /dev/ttyUSB1 - голосовой(при включенном голосовом режиме) интерфейс модема
    // /dev/ttyUSB2 - командный интерфейс модема. Отличается от /dev/ttyUSB0 тем
    // что с него можно читать не только ответы модема на команды, а также служебные
    // сообщения. Такие как данные о качестве сигнала, вывод ^CEND и прочее


    /**
     * @var int
     */
    private $_mode = 1;

    /**
     * @var string
     */
    private $_storage = 'SM';
    /**
     *
     */
    public function __construct()
    {
        // устанавливаем в:
        // 0 - чтобы отключить вывод отладочной информации
        // 1 - чтобы включить вывод отладочной информации
        $this->debug = 0;

        $ports = $this->getDevicePorts();

        if (!file_exists($ports[2])) {
            throw new Exception('Command port is not exists');
        }

        if (!file_exists($ports[1])) {
            throw new Exception('Voice port is not exists');
        }

        $this->command = new GsmAtCommand($ports[2]);
        $this->voice = new GsmVoice($ports[1]);


     }

    /**
     * Получить текущий статус отладка (да/нет)
     *
     * @return bool
     */
    public function debugMode()
    {
        return $this->debug;
    }

    /**
     * Включить режим отладки
     */
    public function debugModeOn()
    {
        $this->debug = true;
        $this->command->debugModeOn();
    }

    /**
     * Выключить режим отладки
     */
    public function debugModeOff()
    {
        $this->debug = false;
        $this->voice->debugModeOff();
    }

    /**
     * Получаем список портов на которых находится GSM Modem
     *
     * @return array
     */
    private function getDevicePorts()
    {
        // обычно это:
        // /dev/ttyUSB0 - командный интерфейс модема
        // /dev/ttyUSB1 - голосовой(при включенном голосовом режиме) интерфейс модема
        // /dev/ttyUSB2 - командный интерфейс модема. Отличается от /dev/ttyUSB0 тем
        // что с него можно читать не только ответы модема на команды, а также служебные
        // сообщения. Такие как данные о качестве сигнала, вывод ^CEND и прочее

        $output = shell_exec('ls -l /dev/serial/by-id');
        preg_match_all("/ttyUSB(\d+):?/i", $output, $matches);

        return [
            '/dev/'.$matches[0][0],
            '/dev/'.$matches[0][1],
            '/dev/'.$matches[0][2],
        ];
    }


    public function getIMEI()
    {
        return $this->command->set('AT+CGSN', '/(.*)/');
    }

    public function reset()
    {
        return $this->command->set('AT+CFUN=1', '/^(OK|ERROR)/');
    }

    /**
     * Установить/Получить режим работы с сообщениями
     * Варианты 'text', 'pdu'
     *
     * @param string $mode
     * @return string
     */
    public function mode($mode=null)
    {
        if ($mode === null) {
            return ($this->_mode) ? 'text' : 'pdu';
        }

        if ($mode === 'text') {
            $mode = 1;
        } else {
            $mode = 0;
        }

        $result = $this->command->set('AT+CMGF='.$mode, '/(OK|ERROR: (\d+))/');

        if ($result === 'OK') {
            $this->_mode = $mode;
        }

        return $result;
    }

    /**
     * Установить/получить текущее место хранеия смс
     * Варианты 'SM', 'ME', 'MT', 'SR'
     *
     * SM – Память SIM-карты
     * ME – Память модема/телефона
     * MT – Это общая память SIM-карты и модема, т.е. MT = SM+ME
     * BM – Память для широковещательных сообщений сети
     * SR – Память для отчетов (о доставке и т.п.)
     *
     * @param string $storage
     * @return string
     * @throws Exception
     */
    public function storage($storage=null)
    {
        if ($storage === null) {
            return $this->_storage;
        }

        $storages = ['SM', 'ME', 'MT', 'SR'];

        if (!in_array($storage, $storages)) {
            throw new \Exception('Unknown storage. Only "'.implode('", "', $storages).'" suported');
        }

        $result = $this->command->set('AT+CPMS="'.$storage.'"', '/(OK|ERROR: (\d+))/');

        if ($result === 'OK') {
            $this->_storage = $storage;
        }

        return $result;
    }

    /**
     * Набор номера
     *
     * @param string $phoneNumber
     */
    public function callNumber($phoneNumber)
    {
        # отдаем модему команду дозвониться до номера $phoneNumber
        # и ожидать ответа от модема:
        # OK - дозвон прошел успешно
        # NO CARRIER - нет соединения с сотовой сетью

        $this->command->set('ATD'.$phoneNumber.';', '/(OK|NO CARRIER)/');
    }

    /**
     * Получение номер СМС центра
     *
     * @return string
     */
    public function smsGetServiceCenter()
    {
        $this->command->set('AT+CSCA?', null);
        $response = $this->command->getAll('/(OK|ERROR)/');

        preg_match('/([\+0-9][0-9]{12})/', $response, $matches);

        return $matches[0];
    }

    /**
     * Устанавливаем номер СМС центра
     *
     * @param $phoneNumber
     * @return string
     */
    public function smsSetServiceCenter($phoneNumber)
    {
       return $this->command->set('AT+CSCA="'.$phoneNumber.'"', '/(OK|ERROR)/');
    }


    /**
     * Список всех смс
     *
     * @return SMS[]
     */
    public function smsGetAll()
    {
        return $this->smsGetByType(4);
    }

    /**
     * Полученные непрочитанные сообщения
     *
     * @return SMS[]
     */
    public function smsGetUnReaded()
    {
        return $this->smsGetByType(0);
    }

    /**
     * Полученные прочитанные сообщения
     *
     * @return SMS[]
     */
    public function smsGetReaded()
    {
        return $this->smsGetByType(1);
    }

    /**
     * Сохраненные неотправленные сообщения
     *
     * @return SMS[]
     */
    public function smsGetUnSented()
    {
        return $this->smsGetByType(2);
    }

    /**
     * Сохраненные отправленные сообщения
     *
     * @return SMS[]
     */
    public function smsGetSented()
    {
        return $this->smsGetByType(3);
    }

    /**
     * Список всех смс
     * http://hardisoft.ru/soft/samodelkin-soft/poluchenie-i-dekodirovanie-sms-soobshhenij-v-formate-pdu/
     *
     * @return SMS[]
     */
    public function smsGetByType($type)
    {
        $this->command->clear();

        $this->mode('pdu');
        $this->command->set('AT+CMGL='.$type, null);

        $response = $this->command->getAll('/^(OK|ERROR|COMMAND NOT SUPPORT)/');

        $response = $this->splitResponse($response);

        unset($response[count($response)-1]);
        unset($response[count($response)-1]);

        $smsList = [];

        if (!$response) {
            return $smsList;
        }

        while($response) {
            $header = array_shift($response);
            $pdu = array_shift($response);

            $smsList[] = new SMS($header, $pdu, $this->storage());
        }

        return $smsList;
    }

    /**
     * @return string
     */
    public function ussd($command)
    {
        return $this->command->set('ATD'.$command, '/(OK|ERROR|ERROR: (\d+)|\+CUSD:)/');
    }

    /**
     * Включаем режим ожидания СМС
     * https://oldlight.wordpress.com/2009/06/16/tutorial-using-at-commands-to-send-and-receive-sms/
     *
     * @return string
     */
    public function smsWaitOn()
    {
        return $this->command->set('AT+CNMI=1,2,0,0,0', '/(OK|ERROR|ERROR: (\d+)|CMT)/');
    }

    /**
     * Ждем входящую СМС и возвращаем ее
     * https://oldlight.wordpress.com/2009/06/16/tutorial-using-at-commands-to-send-and-receive-sms/
     *
     * @return SMS
     */
    public function smsWaitAndRead()
    {
        $handler = $this->command->get('/(\+CMT:)/');
        usleep(200000);
        $result = $this->command->get();

        if ($result) {
            return new SMS($handler, trim($result), $this->storage());
        }
    }

    /**
     * Отправка СМС в текстовом режиме
     *
     * @param string $phoneNumber Номер телефона получателя
     * @param string $text Текст сообщения
     */
    public function smsSendAsTxt($phoneNumber, $text)
    {
        $this->command->set('AT+CMGF=1', '/(OK|ERROR: (\d+))/');
        $this->command->set('AT+CMGS="'.$phoneNumber.'"', null);
        usleep(200000);//0.2 sec
        $this->command->setWithoutEnter($text.chr(26), null);
        usleep(200000);
        echo $this->command->get('/(CMGS|OK|ERROR)/');
    }

    /**
     * Отправка СМС в PDU режиме
     *
     * @param string $phoneNumber Номер телефона получателя
     * @param string $text Текст сообщения
     */
    public function smsSendAsPdu($phoneNumber, $text, $type='normal')
    {
        //Type 'normal' or 'flash'
        $pdu = PduFactory::encode([
            'number' => $phoneNumber,
            'message'=>$text,
            'alphabetSize'=>16,
            'class' => ($type === 'normal') ? -1 : 0xF0,
        ]);

        $this->command->set('AT+CMGF=0', '/(OK|ERROR)/');
        $this->command->set('AT+CMGS='.$pdu['byte_size'], null);
        usleep(200000);//0.2 sec
        $this->command->setWithoutEnter($pdu['message'].chr(26), null);
        usleep(300000);
        echo $this->command->get('/(CMGS|OK|ERROR)/');
    }

    /**
     * Ждем звонка, при входящем звонке должно поступить сообщение RING
     *
     * @return string
     */
    public function waitIncomingСall()
    {
        // при входящем звонке должно поступить сообщение RING
        return $this->command->get('/^(RING)/');
    }

    /**
     * Ожидаем установления соединения
     *
     * @return string
     */
    public function waitCallConnect()
    {
        // ожидаем установления соединения
        return $this->command->get('/^\^??(CONN\:1|CEND\:|ERROR)/');
    }

    /**
     * Ждем сообщения с номером телефона звонящего абонента
     *
     * @return string Номер телефона звонящего
     */
    public function getIncomingPhoneNumber()
    {
        // ждем сообщения с номером телефона звонящего абонента //+CLIP: "+79117654321",145,,,,0
        return $this->command->get('/^\+CLIP\: \"(\+\d+)/');
    }

    /**
     * Принимаем входящий вызов
     *
     * @return mixed
     */
    public function answerThePhone()
    {
        // принимаем входящий вызов
        return $this->command->set('ATA', '/^(OK|ERROR)/');
    }

    /**
     * Переключаем модем в режим приема/передачи голоса
     *
     * @return mixed
     */
    public function setVoiceMode()
    {
        // переключаем модем в режим приема/передачи голоса
        // OK - переключение прошло успешно
        // ERROR - переключение не произведено
        // CEND:.... - абонент недоступен, занят или сбросил вызов
        return $this->command->set('AT^DDSETEX=2', '/(OK|ERROR|CEND\:)/');
    }

    /**
     * Проверка состояния текущего соединения
     *
     * @return mixed
     */
    public function checkConnection()
    {
        // состояние текущего соединения
        // OK - все хорошо идет соединение
        // ERROR - ошибка соединеия
        // CEND - соединение закрыто, разговор окончен
        return $this->command->set("AT+CLCC", "/^\^??(OK|ERROR|CEND)/");
    }

    /**
     * Вешаем трубку
     *
     * @return mixed
     */
    public function endCall()
    {
        return $this->command->set("AT+CHUP");
    }

    public function getDeviveModel()
    {
        $this->command->set("AT+CGMM", null);

        $response = $this->command->getAll('/(OK)/');

        return $this->splitResponse($response)[2];
    }

    public function getNetwork()
    {
        $this->command->set("AT+COPS?", null);

        $response = $this->command->getAll('/(OK)/');

        preg_match('/"([^"]*)"/', $response, $matches);

        if(!isset($matches[1])) {
            return 'Unknown';
        }

        return Networks::getNameById($matches[1]);
    }

    private function splitResponse($response)
    {
        return preg_split("/\r\n|\n|\r/", trim($response));
    }
}
