<?php
namespace StelsSoft\GSM;

class GsmVoice extends GsmBasePort
{
    /**
     * Запись данных в звуковой поток модема
     *
     * @param $data
     */
    public function write($data)
    {
        fwrite($this->handle, $data);
    }

    /**
     * Чтение данных з модема
     *
     * @param $size int Количество читаемых байт
     * @return string
     */
    public function read($size)
    {
        return fread($this->handle, $size);
    }
}