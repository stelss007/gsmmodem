<?php
namespace StelsSoft\GSM;

/**
 * Class GsmBasePort
 * http://www.gsmforum.ru/threads/96779-%D0%A1%D0%BF%D1%80%D0%B0%D0%B2%D0%BE%D1%87%D0%BD%D0%B8%D0%BA-%D0%BF%D0%BE-%D0%90%D0%A2-%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D0%B0%D0%BC
 * http://diff.org.ua/archives/1583
 * http://www.g-news.com.ua/forum_smf/index.php?topic=10489.0
 * http://tails-up.blogspot.com/2012/07/sms-gsm-c.html
 * http://we.easyelectronics.ru/part/gsm-gprs-modul-sim900-chast-vtoraya.html
 *
 * @package StelsSoft\GSM
 */
class GsmBasePort
{
    /**
     * @var resource
     */
    protected $handle;

    /**
     * Флаг режима отладки
     *
     * @var boolean
     */
    protected $debug;

    /**
     * @param $port
     */
    public function __construct($port = null)
    {
        $this->handle = fopen($port, 'r+b');
    }

    /**
     *
     */
    public function __destruct()
    {
        fclose($this->handle);
    }

    /**
     * Получить текущий статус отладка (да/нет)
     *
     * @return bool
     */
    public function debugMode()
    {
        return $this->debug;
    }

    /**
     * Включить режим отладки
     */
    public function debugModeOn()
    {
        $this->debug = true;
    }

    /**
     * Выключить режим отладки
     */
    public function debugModeOff()
    {
        $this->debug = false;
    }


}