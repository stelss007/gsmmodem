<?php
namespace StelsSoft\GSM;

class GsmAtCommand extends GsmBasePort
{
    /**
     * Получаем данные от модема по заданому регулярному выражению
     *
     * @param $regexp
     * @return mixed
     */
    public function get($regexp=null)
    {
        if ($regexp === null) {
            return fgets($this->handle);
        }
        $receive = '';

        while(!preg_match($regexp, $receive, $matches)) {
            $receive = fgets($this->handle);

            if ($this->debug && $receive) {
                echo 'RECIVE: [' . trim($receive) . ']'.PHP_EOL;
                flush();
            }

            //usleep(200000);//0.2 sec
        }

        return $matches[1];
    }

    /**
     * Получаем данные от модема по заданому регулярному выражению
     *
     * @param $regexp
     * @return mixed
     */
    public function getAll($regexp)
    {
        $receive = '';
        $receiveResult = '';

        while(!preg_match($regexp, $receive, $matches)) {
            $receive = fgets($this->handle);
            $receiveResult .= $receive;

            if ($this->debug && $receive) {
                echo 'RECIVE: [' . trim($receive) . ']'.PHP_EOL;
                flush();
            }

            //usleep(200000);//0.2 sec
        }

        return $receiveResult;
    }

    /**
     * Отправляем модему команду и ждем ответа (указываем в регулярном выражении)
     *
     * @param $cmd
     * @param string $regexp
     * @return mixed
     */
    public function set($cmd, $regexp = '/(OK)/')
    {
        fwrite($this->handle, "$cmd\r");

        if ($this->debug) {
            echo 'SEND: [' . trim($cmd) . ']'.PHP_EOL;
        }

        if ($regexp === null) {
            return true;
        }

        return $this->get($regexp);
    }

    /**
     * Отправляем модему команду и ждем ответа (указываем в регулярном выражении)
     *
     * @param $cmd
     * @param string $regexp
     * @return mixed
     */
    public function setWithoutEnter($cmd, $regexp = '/(OK)/')
    {
        fwrite($this->handle, "$cmd");

        if ($this->debug) {
            echo 'SEND: [' . trim($cmd) . ']'.PHP_EOL;
        }

        if ($regexp === null) {
            return true;
        }

        return $this->get($regexp);
    }

    /**
     * Очистим команды, ели что-то зависло
     */
    public function clear()
    {
        $this->set('AT', '/(OK|ERROR)/');
        return true;
    }
}