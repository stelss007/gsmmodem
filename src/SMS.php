<?php
namespace StelsSoft\GSM;

use StelsSoft\PDU\PduFactory;

class SMS
{
    private $header;
    private $pdu;
    private $storage;
    private $smsInfo;

    public function __construct($header, $pdu, $storage)
    {
        $this->header = $header;
        $this->pdu = $pdu;
        $this->storage = $storage;

        $this->smsInfo = PduFactory::decode($this->pdu);
    }

    /**
     *
     * @return string
     */
    public function getDateTime()
    {
        return $this->smsInfo['timeStamp'];
    }

    /**
     * Возвращаем Номер/имя отправителя
     * @return string
     */
    public function getSender()
    {
        return $this->smsInfo['number'];
    }

    /**
     * Возвращаем текст СМС
     * @return string
     */
    public function getContent()
    {
        return $this->smsInfo['message'];
    }

    /**
     * Возвращаем всю информацию о СМС (декодировано с PDU)
     * @return string
     */
    public function getInfo()
    {
        return $this->smsInfo;
    }
}