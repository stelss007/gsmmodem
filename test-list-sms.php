<?php

include_once('./DtmfDecoder.php');
include_once('./Networks.php');
include_once('./GsmModem.php');

include_once('./PDU/InvalidArgumentException.php');
include_once('./PDU/Utf8.php');
include_once('./PDU/PduClass.php');
include_once('./PDU/PduFactory.php');

use StelsSoft\GSM\GsmModem;

$modem = new GsmModem();
$modem->debugModeOn();

//Память SIM-Card и память устройства
$modem->storage('MT');

////Память SIM-Card
//$modem->storage('SM');

//Получаем список всех входящих сообщений из заданого хранилища $modem->storage('MT');
$messages = $modem->smsGetAll();

//Перебераем все сообщения и выводим на экран
foreach ($messages as $msg) {
    echo $msg->getSender().PHP_EOL;
    echo $msg->getContent().PHP_EOL;
    echo '--------------------'.PHP_EOL;
}


//Включаем режим ожидания входящих сообщений
$modem->smsWaitOn();

//Запускаем бесконечный цыкл для ожидания Новых СМС
while(true){
    // Вернет новую СМС
    $sms = $modem->smsWaitAndRead();
    //Выводим СМС
    echo $sms->getSender().PHP_EOL;
    echo $sms->getContent().PHP_EOL;
    usleep(200000);//0.2 sec
}